<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
<li><a href="{{ backpack_url('dashboard') }}"><i class="fa fa-dashboard"></i> <span>{{ trans('backpack::base.dashboard') }}</span></a></li>
<li><a href='{{ backpack_url('flat') }}'><i class='fa fa-tag'></i> <span>Квартиры</span></a></li>
<li><a href='{{ backpack_url('layout') }}'><i class='fa fa-tag'></i> <span>Планировки</span></a></li>
<li><a href='{{ backpack_url('construction') }}'><i class='fa fa-building'></i> <span>Дома</span></a></li>
<li><a href='{{ backpack_url('buildings') }}'><i class='fa fa-building'></i> <span>Объекты</span></a></li>