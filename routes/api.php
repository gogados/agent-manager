<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
    Route::get('construction', 'Api\ConstructionController@index');
// Route::group([
//     'middleware' => ['web', config('backpack.base.middleware_key', 'admin')]
// ], function () { // custom admin routes

// Route::get('construction', 'Api\ConstructionController@index');
// }); // this should be the absolute last line of this file



// Route::get('construction/', ['middleware' => 'csrf', 'uses' => 'Api\ConstructionController@index']);
// Route::get('construction/{id}', ['middleware' => 'csrf', 'uses' => 'Api\ConstructionController@show']);
// Route::get('construction', array('before' => 'csrf', function(){ 
//         Route::get('construction', 'Api\ConstructionController@index');
//         Route::get('construction/{id}', 'Api\ConstructionController@show');
// }));







Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
