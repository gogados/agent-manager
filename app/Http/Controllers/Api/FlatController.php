<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \App\Models\Flat;

class FlatController extends Controller
{
    public function index(Request $request)
    {
        $constrId = $request->get('construction');
        return Flat::whereHas('layout', function ($query) {
          $query->where('construction_id', '=', $constrId);
        })->get();
    }

    public function show($id)
    {
        return Flat::find($id);
    }
}