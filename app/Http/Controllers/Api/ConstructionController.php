<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \App\Models\Construction;
use Session;



class ConstructionController extends Controller
{


    public function index(Request $request)
    {
      $bid = 0;
      $isAuth = false;
      $session = $request->cookie('name');
      foreach($request->input('form') as $obj)
      {
        if($obj['name'] == '_token') //< полная хрень middleware bacpack не хочет работать
        {
          if($obj['value'])
          {      
            $isAuth = true;
          }
        }
        if($obj['name'] == 'building_id')
          $bid = $obj['value'];
        
      }
      if(!$isAuth)
      {
        abort(403);
      }
      $ownBuildings = Construction::where('building_id','=',$bid)->paginate(10);
      $selectArray = [];
      foreach( $ownBuildings as $value){
        array_push($selectArray,
        array("id"=>$value['construction'], "text"=>$value['name']));
      }
      return $ownBuildings;
    }

    public function show($id)
    {
      return Construction::find($id);
    }
}
