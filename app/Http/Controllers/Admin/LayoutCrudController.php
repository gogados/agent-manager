<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\LayoutRequest as StoreRequest;
use App\Http\Requests\LayoutRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;
use \App\Models\Buildings;
use \App\Models\Construction;
use \App\Models\Layout;

/**
 * Class LayoutCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class LayoutCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->admin_id = \Auth::guard('backpack')->user()->id;
        $this->crud->setModel('App\Models\Layout');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/layout');
        $this->crud->setEntityNameStrings('layout as l', 'layouts');
        $this->crud->addClause('whereIn', 'construction_id' , function ($query) {
          $query->select('construction')
          ->from('constructions')
          ->whereIn('building_id', function($query)
          {
            $query->select('building')
            ->from('buildings')
            ->where('builder_id', '=', $this->admin_id);
          }); 
        });

        $this->crud->setListView('custom');
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        $ownBuildings = Buildings::where('builder_id','=',$this->admin_id)->get();
        $this->selectArray = [];
        foreach( $ownBuildings as $value){
          $this->selectArray[strval($value->building)] = $value->building_name;
        }

//'photo','square','count_rooms','square_rooms','square_kitchen'
        // TODO: remove setFromDb() and manually define Fields and Columns
        $this->crud->addColumns([
          [
            // 1-n relationship
            'label' => "Дом", // Table column heading
            'type' => "select",
            'name' => 'construction_id', // the column that contains the ID of that connected entity;
            'entity' => 'construction', // the method that defines the relationship in your Model
            'attribute' => "name", // foreign key attribute that is shown to user
            'model' => "App\Models\Construction", // foreign key model
          ],
          [ 
          'label' => "Фото", 
          'type' => "image",
          'name' => 'photo',
          'height' => '200px',
          'width' => '200px',
        ],
        [ 
          'label' => "Площадь", 
          'type' => "number",
          'name' => 'square',
          'attributes' => ["step" => "any"],
          'min' => '0'
        ],
        [ 
          'label' => "Количество комнат", 
          'type' => 'select2_from_array',
          'options' => [
            0 =>  'Студия',
            1 =>  '1',
            2 =>  '2',
            3 =>  '3',
            4 =>  '4',
            5 =>  '5',
            6 =>  '6'
          ],
          'name' => 'count_rooms'
        ],
        [ 
          'label' => "Площадь комнат", 
          'type' => "number",
          'name' => 'square_rooms',
          'attributes' => ["step" => "any"],
          'min' => '0'
        ],
        [ 
          'label' => "Площадь кухни", 
          'type' => "number",
          'name' => 'square_kitchen',
          'attributes' => ["step" => "any"],
          'min' => '0'
        ]
        ]
      );
         $this->crud->addFields([
          [ // select_from_array
            'name' => 'building_id',
            'label' => "Объект",
            'type' => 'select2_from_array',
            'options' => $this->selectArray,
            'allows_null' => false
            // 'allows_multiple' => true, // OPTIONAL; needs you to cast this to array in your model;
          ],
          [
            // 1-n relationship
            'label' => 'Дом', // Table column heading
            'type' => "select2_from_ajax",
            'name' => 'construction_id', // the column that contains the ID of that connected entity
            'entity' => 'construction', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'model' => "App\Models\Construction", // foreign key model
            'data_source' => url("api/construction"), // url to controller search function (with /{id} should return model)
            'placeholder' => 'Select a category', // placeholder for the select
            'minimum_input_length' => 0, // minimum characters to type before querying results
            'dependencies' => ['building_id'], // when a dependency changes, this select2 is reset to null
            // ‘method'                    => ‘GET’, // optional - HTTP method to use for the AJAX call (GET, POST)
            // 'include_all_form_fields'  => false, // optional - only send the current field through AJAX (for a smaller payload if you're not using multiple chained select2s)
          ],
           [
          'name' => 'photo',
          'type' => 'image',
          'label' => "photo",
          'crop' => true
        ],
        [ 
          'label' => "Площадь", 
          'type' => "number",
          'name' => 'square',
          'min' => '0',
          'attributes' => ["step" => "any"]
        ],
        [ 
          'label' => "Количество комнат", 
          'type' => 'select2_from_array',
          'options' => [
            0 =>  'Студия',
            1 =>  '1',
            2 =>  '2',
            3 =>  '3',
            4 =>  '4',
            5 =>  '5',
            6 =>  '6'
          ],
          'name' => 'count_rooms'
        ],
        [ 
          'label' => "Площадь комнат", 
          'type' => "number",
          'name' => 'square_rooms',
          'attributes' => ["step" => "any"],
          'min' => '0'
        ],
        [ 
          'label' => "Площадь кухни", 
          'type' => "number",
          'name' => 'square_kitchen',
          'attributes' => ["step" => "any"],
          'min' => '0'
        ]]);
        $this->crud->allowAccess('show');
        $this->crud->addFilter([ // select2 filter
          'name' => "buildings",
          'type' => 'select2',
          'label'=> 'Объект'
        ], function() {
            return $this->selectArray;
        }, function($value) { // if the filter is active
            $this->crud->addClause('whereIn', 'construction_id' , function ($query) use($value) {
          $query->select('construction')
          ->from('constructions')
          ->where('building_id','=',$value);
        });
      });
    }
    public function hasAccess($id)
    {
      $layout = Layout::where('id','=',$id)->whereIn('construction_id',function($query)
      {
          $query->select('construction')
          ->from('constructions')
          ->whereIn('building_id', function($query)
          {
            $query->select('building')
            ->from('buildings')
            ->where('builder_id', '=', $this->admin_id);
          });
      })->count();
      return $layout > 0;
    }

    public function show($id)
    {
      if($this->hasAccess($id))
      {
        return parent::show($id);
      }
      abort(403);
    }

    public function edit($id)
    {
      if($this->hasAccess($id))
      {
        return parent::edit($id);
      }
      abort(403);
    }

    public function destroy($id)
    {
      if($this->hasAccess($id))
      {
        return parent::destroy($id);
      }
      abort(403);
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
