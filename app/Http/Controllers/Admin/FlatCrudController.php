<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;
use Illuminate\Http\Request;
// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\FlatRequest as StoreRequest;
use App\Http\Requests\FlatRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;
use \App\Models\Buildings;
use \App\Models\Construction;
use \App\Models\Flat;
use \App\Models\Layout;
use \App\Models\FlatStatus;
/**
 * Class FlatCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class FlatCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->admin_id = \Auth::guard('backpack')->user()->id;
        $this->crud->setModel('App\Models\Flat');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/flat');
        $this->crud->setEntityNameStrings('flat', 'flats');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        $this->crud->setListView('flats');

        $ownBuildings = Buildings::where('builder_id','=',$this->admin_id)->get();
        $selectArray = [];
        foreach( $ownBuildings as $value){
          $selectArray[strval($value->building)] = $value->building_name;
        }
        $this->ownBuildings = $selectArray;
        $this->crud->addColumn([ 
          'label' => "Тип",
          'type' => "number",
          'name' => 'type'
         ]);   
        $constructionId = \Request::has('construction')? \Request::get('construction') : '-1';
        $number = \Request::has('number')? \Request::get('number') : '-1';
        $lvl = \Request::has('lvl')? \Request::get('lvl') : '-1';
        $porch = \Request::has('porch')? \Request::get('porch') : '-1';
        $layouts = Layout::where('construction_id','=',$constructionId)->get();
        $status = FlatStatus::all();
        $selectArray = [];
        foreach( $layouts as $value){
          $selectArray[strval($value->id)] = $value->square.'м², комнат: '.$value->count_rooms;
        }
        $selectStatusArray = [];
        foreach( $status as $value){
          $selectStatusArray[strval($value->id)] = $value->status_name;
        }
        $this->crud->addFields(
        [
        [ 
          'label' => "contruction_id",
          'type'  => 'hidden',
          'name' => 'contruction_id',
          'default' => $constructionId
        ],
        [ 
          'label' => "number",
          'type'  => 'hidden',
          'name' => 'number',
          'default' => $number
        ],
        [ 
          'label' => "Номер",
          'type'  => 'number',
          'attributes' => ['disabled' => 'disabled'],
          'name' => 'fake_number',
          'default' => $number
        ],
        [ // select_from_array
            'name' => 'layout_id',
            'label' => 'Планировка',
            'type' => 'select_from_array',
            'options' => $selectArray,
            'allows_null' => false
            // 'allows_multiple' => true, // OPTIONAL; needs you to cast this to array in your model;
        ],
        [ // select_from_array
            'name' => 'status_id',
            'label' => 'Статус',
            'type' => 'select_from_array',
            'options' => $selectStatusArray,
            'allows_null' => false
            // 'allows_multiple' => true, // OPTIONAL; needs you to cast this to array in your model;
        ],
          [
            'name' => 'level', // the db column name (attribute name)
            'label' => "Этаж", // the human-readable label for it
            'type' => 'hidden', // the kind of column to show
            'default' => $lvl
          ],
          [
            'name' => 'porch', // the db column name (attribute name)
            'label' => "Подъезд", // the human-readable label for it
            'type' => 'hidden', // the kind of column to show
            'default' => $porch
          ],
          [
            'name' => 'price', // the db column name (attribute name)
            'label' => "Цена", // the human-readable label for it
            'type' => 'number' // the kind of column to show
          ],

        ]);  

        $this->crud->addFilter([ // select2 filter
          'name' => "construction",
          'type' => 'select2',
          'label'=> 'Объект'
        ], function() {
            return $this->ownBuildings;
        }, function($value) { // if the filter is active
            // $this->crud->addClause('where', 'status', $value);
        });
        $this->crud->allowAccess('search');
        $this->crud->setCreateView('createFlat');
        // add asterisk for fields that are required in FlatRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
        //$this->crud->allowAccess('show');
    }
    public function hasAccess($id)
    {
      $layout = Flat::where('id','=',$id)->whereIn('layout_id',function($query)
      {
          $query->select('id')
          ->from('layout')
          ->whereIn('construction_id', function($query)
          {
            $query->select('construction')
            ->from('constructions')
            ->whereIn('building_id', function($query)
            {
              $query->select('building')
              ->from('buildings')
              ->where('builder_id', '=', $this->admin_id);
            });
          });
      })->count();
      return $layout > 0;
    }

    public function show($id)
    {
      if($this->hasAccess($id))
      {
        return parent::show($id);
      }
      abort(403);
    }

    public function edit($id)
    {
      if($this->hasAccess($id))
      {
        return parent::edit($id);
      }
      abort(403);
    }

    public function destroy($id)
    {
      if($this->hasAccess($id))
      {
        return parent::destroy($id);
      }
      abort(403);
    }

    public function search()
    {
      $result = parent::search();
      $result['construction_id'] = $this->crud->getFilter('construction')->currentValue;
      if(\Request::has('number'))
      {
      $number = \Request::get('number');
      $result['data'] = Flat::whereHas('layout', function ($query) use($result,$number) {
                                  $query->where('construction_id', '=', $result['construction_id'])
                                   ->where('number','=',$number)
                                   ->whereIn('layout_id', function($query)
                                   {
                                     $query->select('id')
                                     ->from('layout')
                                     ->whereIn('construction_id' , function ($query) {
                                        $query->select('construction')
                                        ->from('constructions')
                                        ->whereIn('building_id', function($query)
                                        {
                                          $query->select('building')
                                          ->from('buildings')
                                          ->where('builder_id', '=', $this->admin_id);
                                        }); 
                                      });
                                   });
                                })->with('layout')->with('status')->get();        
      }
      else
      {
      $result['data'] = Flat::whereHas('layout', function ($query) use($result) {
                                  $query->where('construction_id', '=', $result['construction_id'])
                                  ->whereIn('construction_id' , function ($query) {
                                        $query->select('construction')
                                        ->from('constructions')
                                        ->whereIn('building_id', function($query)
                                        {
                                          $query->select('building')
                                          ->from('buildings')
                                          ->where('builder_id', '=', $this->admin_id);
                                        }); 
                                      });
                                })->with('status')->get();
      }
      return $result;
    }

    public function store(StoreRequest $request)
    {
        $input = \Request::all();
        if(isset($input['templates']))
        {
          $templates = $input['templates'];
          $sizeLvl = count($templates);
          $flats = $input['flats'];
          foreach ($flats as $key => $flat) {
            if($flat['exist'])
            {
              continue;
            }
            $index = ($key) % $sizeLvl;
            $templ_ = $templates[$index];
            $templ_['level'] = $flat['level'];
            $templ_['porch'] = $flat['porch'];
            $templ_['number'] = $flat['number'];
            $this->crud->create($templ_);
          }
          return "success";
        }
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
