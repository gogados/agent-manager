<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\BuildingsRequest as StoreRequest;
use App\Http\Requests\BuildingsRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\Auth\Guard;
use \App\Models\Buildings;

/**
 * Class BuildingsCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class BuildingsCrudController extends CrudController
{

    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->admin_id = \Auth::guard('backpack')->user()->id;
        $this->crud->setModel('App\Models\Buildings');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/buildings');
        $this->crud->setEntityNameStrings('buildings', 'buildings');
        $this->crud->addClause('where', 'builder_id', '=', $this->admin_id);

        $this->crud->setListView('custom');
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */
        $this->crud->setListContentClass('col-md-12');
        // TODO: remove setFromDb() and manually define Fields and Columns
        $this->crud->addColumn([ 
          'label' => "Имя",
          'type' => "text",
          'name' => 'building_name'
         ]);    
         $this->crud->addColumn([ 
          'name' => 'address',
          'type' => 'text',
          'label' => "Адрес новостройки"
        ]);
         $this->crud->addColumn([ 
          'label' => "Фото", 
          'type' => "image",
          'name' => 'photo',
          'height' => '200px',
          'width' => '200px',
         ]);
         $this->crud->addColumn([ 
           'name' => 'year_completion',
           'type' => 'date',
           // optional:
           'format' => 'Y',
           'label' => "Год окончания строительства"
         ]);
         $this->crud->addColumn([ 
          'name' => 'quarter_completion',
          'type' => 'select_from_array',
          'options' => ['1' => 'Первый', '2' => 'Второй','3' => 'Третий', '4' => 'Четвёртый'],
          'label' => "Квартал окончания строительства"
        ]);

         $this->crud->addColumn([ // Table
          'name' => 'description',
          'label' => 'Описание',
          'type' => 'tinymce',
          ]
          );




        $this->crud->addField([
          'name' => 'building_name',
          'type' => 'text',
          'label' => "Имя"
        ]);
        $this->crud->addField([
          'name' => 'address',
          'type' => 'address_algolia',
          'label' => "Адрес новостройки"
        ]);
        $this->crud->addField([
          'name' => 'year_completion',
          'type' => 'datetime_picker',
          // optional:
          'datetime_picker_options' => [
              'format' => 'YYYY',
              'language' => 'fr'
          ],
          'label' => "Год окончания строительства"
        ]);

        $this->crud->addField([
          'name' => 'quarter_completion',
          'type' => 'select_from_array',
          'options' => ['1' => 'Первый', '2' => 'Второй','3' => 'Третий', '4' => 'Четвёртый'],
          'allows_null' => false,
          'default' => '1',
          'label' => "Квартал окончания строительства"
        ]);

         $this->crud->addField( 
           [
          'name' => 'description',
          'label' => 'Описание',
          'type' => 'tinymce'
          ]);

        $this->crud->addField([
          'name' => 'photo',
          'type' => 'image',
          'label' => "photo",
          'crop' => true
        ]);

        $this->crud->allowAccess('show');

        // $this->crud->addField([
        //   'name' => 'photo',
        //   'label' => 'photo',
        //   'type' => 'image',
        //   'upload' => true,
        //   'crop' => true
        // ]);
        
    }

    public function hasAccess($id)
    {
      $layout = Buildings::where('building','=',$id)->where('builder_id','=',$this->admin_id)->count();
      return $layout > 0;
    }

    public function show($id)
    {
      if($this->hasAccess($id))
      {
        return parent::show($id);
      }
      abort(403);
    }

    public function edit($id)
    {
      if($this->hasAccess($id))
      {
        return parent::edit($id);
      }
      abort(403);
    }

    public function destroy($id)
    {
      if($this->hasAccess($id))
      {
        return parent::destroy($id);
      }
      abort(403);
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $request->request->set('builder_id',$this->admin_id);
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }


}
