<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\ConstructionRequest as StoreRequest;
use App\Http\Requests\ConstructionRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;
use Illuminate\Support\Facades\DB;
use \App\Models\Buildings;
use \App\Models\Construction;
/**
 * Class ConstructionCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class ConstructionCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->admin_id = \Auth::guard('backpack')->user()->id;
        $this->crud->setModel('App\Models\Construction');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/construction');
        $this->crud->setEntityNameStrings('constructions', 'Дома');
        $this->crud->addClause('join', 'buildings as b' , function ($join) {
          $join->on('b.building', '=', 'constructions.building_id')
               ->where('b.builder_id', '=', $this->admin_id);
        });
        
        $this->crud->setListView('custom');

        // $this->crud->query = $this->crud->query
        //                     ->join('buildings', function ($join) {
        //                       $join->on('buildings.id', '=', 'construction.building_id')
        //                            ->where('buildings.builder_id', '=', $this->admin_id);
        //                     })
        //                     ->select('construction.*', 'buildings.builder_id');
        // $this->crud->addClause('where', 'buildings.builder_id', '=', $this->admin_id);
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */
        $ownBuildings = Buildings::where('builder_id','=',$this->admin_id)->get();
        $selectArray = [];
        foreach( $ownBuildings as $value){
          $selectArray[strval($value->building)] = $value->building_name;
        }
        $this->crud->addColumns([
          [ // select_from_array
            'name' => 'building_id',
            'label' => 'Объект',
            'type' => 'select_from_array',
            'options' => $selectArray,
            'allows_null' => false
            // 'allows_multiple' => true, // OPTIONAL; needs you to cast this to array in your model;
          ],
          [
            'name' => 'name', // the db column name (attribute name)
            'label' => "Имя", // the human-readable label for it
            'type' => 'text' // the kind of column to show
          ],
          [
            'name' => 'number', // the db column name (attribute name)
            'label' => "Номер дома", // the human-readable label for it
            'type' => 'number' // the kind of column to show
          ],
          [
            'name' => 'count_lvl', // the db column name (attribute name)
            'label' => "Количество этажей", // the human-readable label for it
            'type' => 'number' // the kind of column to show
          ],
          [
            'name' => 'count_porch', // the db column name (attribute name)
            'label' => "Количество подъездов", // the human-readable label for it
            'type' => 'number' // the kind of column to show
          ],
          [ 
            'name' => 'year_completion',
            'type' => 'date',
            // optional:
            'format' => 'Y',
            'label' => "Год окончания строительства"
          ],
          [ 
            'name' => 'quarter_completion',
            'type' => 'select_from_array',
            'options' => ['1' => 'Первый', '2' => 'Второй','3' => 'Третий', '4' => 'Четвёртый'],
            'label' => "Квартал окончания строительства"
          ],
          [   // Checkbox
            'name' => 'completed',
            'label' => 'Дом сдан',
            'type' => 'check'
          ],
          ]);

        // TODO: remove setFromDb() and manually define Fields and Columns
        $this->crud->addFields(
          [
            [ // select_from_array
              'name' => 'building_id',
              'label' => 'Объект',
              'type' => 'select2_from_array',
              'options' => $selectArray,
              // 'allows_multiple' => true, // OPTIONAL; needs you to cast this to array in your model;
            ] ,  
            [
              'name' => 'name', // the db column name (attribute name)
              'label' => "Имя", // the human-readable label for it
              'type' => 'text' // the kind of column to show
            ],
            [
              'name' => 'number', // the db column name (attribute name)
              'label' => "Номер дома", // the human-readable label for it
              'type' => 'number' // the kind of column to show
            ],
            [
              'name' => 'count_lvl', // the db column name (attribute name)
              'label' => "Максимальное количество этажей", // the human-readable label for it
              'type' => 'number' // the kind of column to show
            ],
            [ // Table
              'name' => 'count_flats',
              'label' => 'Количество квартир на этаже',
              'type' => 'table',
              'entity_singular' => 'параметр', // used on the "Add X" button
              'columns' => [
                  'porch' => 'Подъезд',
                  'lvlb' => 'c',
                  'lvle' => 'по',
                  'count' => 'количество'
              ],
              'min' => 0, // minimum rows allowed in the table
            ],
            [
              'name' => 'count_porch', // the db column name (attribute name)
              'label' => "Количество подъездов", // the human-readable label for it
              'type' => 'number' // the kind of column to show
            ],
            [
              'name' => 'year_completion',
              'type' => 'datetime_picker',
              // optional:
              'datetime_picker_options' => [
                  'format' => 'YYYY',
                  'language' => 'fr'
              ],
              'label' => "Год окончания строительства"
            ],
            [ 
              'name' => 'quarter_completion',
              'type' => 'select_from_array',
              'options' => ['1' => 'Первый', '2' => 'Второй','3' => 'Третий', '4' => 'Четвёртый'],
              'label' => "Квартал окончания строительства"
            ],
            [   // Checkbox
              'name' => 'completed',
              'label' => 'Дом сдан',
              'type' => 'checkbox'
            ]
          ]);

      $this->crud->allowAccess('show');
      $this->crud->setRequiredFields(StoreRequest::class, 'create');
      $this->crud->setRequiredFields(UpdateRequest::class, 'edit');

    }
    public function hasAccess($id)
    {
      $layout = Construction::where('construction','=',$id)->whereIn('building_id',function($query)
      {
            $query->select('building')
            ->from('buildings')
            ->where('builder_id', '=', $this->admin_id);
      })->count();
      return $layout > 0;
    }

    public function show($id)
    {
      if($this->hasAccess($id))
      {
        return parent::show($id);
      }
      abort(403);
    }

    public function edit($id)
    {
      if($this->hasAccess($id))
      {
        return parent::edit($id);
      }
      abort(403);
    }

    public function destroy($id)
    {
      if($this->hasAccess($id))
      {
        return parent::destroy($id);
      }
      abort(403);
    }

    public function search()
    {
        if(\Request::has('id'))
        {
            $constructionId = \Request::has('id')? \Request::get('id') : '-1';
            return Construction::where('construction','=',$constructionId)
            ->whereIn('building_id', function($query){
              $query->select('building')
              ->from('buildings')
              ->where('builder_id','=',$this->admin_id);
            })->first();
        }
        if(\Request::has('withBuildings'))
        {
          return Construction::join('buildings as b', function($join)
          {
            $join->on('b.building', '=', 'building_id');
            // ->where('b.builder_id','=',$this->admin_id);
          })->get(); 
        }
        return parent::search();
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        // $countFlats = json_decode($request->request->get('count_flats'));
        // $countPorch = $request->request->get('count_porch');
        // $countLvl = $request->request->get('count_lvl');
        // $number = 1;
        // for($porch=0;$porch < $countPorch;$countPorch++)
        // {
        //   foreach ($countFlats as $key => $range) 
        //   {
        //     for($lvl = $range->lvlb; $lvl <= $range->lvle; $range++)
        //     {
        //       for($i = 0; $i < $range->count; $i++)
        //       {
        //         $flat = new Flat();
        //         $flat->number = $number;
        //         $flat->lvl = $lvl;
        //         $flat->porch = $porch;
        //         $flat->save();
        //         $number++;
        //       }
        //     }
        //   }
        // }
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
